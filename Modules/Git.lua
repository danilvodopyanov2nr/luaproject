if not fs.exists("SomeProgram.lua") then
  local response = http.get(
    "https://gitlab.com/api/v4/projects/56947420/repository/files/Programs%2FSomeProgram.lua/raw?ref=master")

  if response then
    local content = response.readAll()
    response.close()

    local file = fs.open("Modules/Git.lua", "w")
    file.write(content)
    file.close()
  else
    print("Failed to download SomeProgram.lua")
  end
end

local Git = require("Modules.Git")
