-- "https://gitlab.com/api/v4/projects/30860168/repository/files/Modules%2FGit.lua/raw?ref=master"
local logger = require("Modules.Logger")
if not fs.exists("luaproject/SomeProgram.lua") then
  local response, err, failure = http.get(
    "https://gitlab.com/api/v4/projects/56973986/repository/files/Programs%2FSomeProgram.lua/raw?ref=main")

  logger.logNoTag(response, err, failure)
  if response then
    local content = response.readAll()
    response.close()

    local file = fs.open("luaproject/SomeProgram.lua", "w")
    file.write(content)
    file.close()
  else
    print("Failed to download SomeProgram.lua")
  end
end
